<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>XSL Transform</title>
                <meta charset="UTF-8" />
                <link rel="stylesheet" href="style.css"/>
            </head>
            <a href="https://www.uefa.com/uefachampionsleague/history/seasons/2023/">
            <img class="logo" src="logo_UEFA.png"/></a>
        </html>
        <body>
        <h1>UEFA Champions League</h1>
        <table>
            <tr>
                <th colspan="7">Quarter finals</th>
                <th>Winner</th>
            </tr>
            <xsl:for-each select="UEFA/season/quarter-finals/match">
            <tr>
                <td><xsl:variable name="logotip" select="first-leg/local/logo"/>
                    <img src="{$logotip}"/></td>
                <td>
                    <xsl:value-of select="first-leg/local/name"/>
                </td>
                <td>
                    <xsl:value-of select="first-leg/local/goals"/>
                </td>
                <td>-</td>
                <td>
                    <xsl:value-of select="first-leg/visitant/goals"/>
                </td>
                <td>
                    <xsl:value-of select="first-leg/visitant/name"/>
                </td>
                <td><xsl:variable name="logotip" select="first-leg/visitant/logo"/>
                    <img src="{$logotip}"/></td>    
            </tr>
            <tr>
                <td><xsl:variable name="logotip" select="second-leg/local/logo"/>
                    <img src="{$logotip}"/></td>
                <td>
                    <xsl:value-of select="second-leg/local/name"/>
                </td>
                <td>
                    <xsl:value-of select="second-leg/local/goals"/>
                </td>
                <td>-</td>
                <td>
                    <xsl:value-of select="second-leg/visitant/goals"/>
                </td>
                <td>
                    <xsl:value-of select="second-leg/visitant/name"/>
                </td>
                <td><xsl:variable name="logotip" select="second-leg/visitant/logo"/>
                    <img src="{$logotip}"/></td>    
            </tr>
            </xsl:for-each>
        </table>
        <p>Total goals: <xsl:value-of select="sum(UEFA/season/quarter-finals/match/first-leg/local/goals) + sum(UEFA/season/quarter-finals/match/first-leg/visitant/goals) + sum(UEFA/season/quarter-finals/match/second-leg/local/goals) + sum(UEFA/season/quarter-finals/match/second-leg/visitant/goals)"/></p>
        <p>Number of matches: <xsl:value-of select="count(UEFA/season/quarter-finals/match) * 2"/></p>
        <p>Avarage goals per match: <xsl:value-of select="(sum(UEFA/season/quarter-finals/match/first-leg/local/goals) + sum(UEFA/season/quarter-finals/match/first-leg/visitant/goals) + sum(UEFA/season/quarter-finals/match/second-leg/local/goals) + sum(UEFA/season/quarter-finals/match/second-leg/visitant/goals)) div (count(UEFA/season/quarter-finals/match) * 2) "/></p>
        </body>
    </xsl:template>
</xsl:stylesheet>