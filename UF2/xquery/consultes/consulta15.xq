let $doc := doc('institut.xml')
return
<school>
{
  for $i in $doc/institut/alumnes/alumne
  let $j := $i/edat
  order by $i/nom descending
  return <alm>~{$j/*}</alm>
}
</school>