<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>Alumnes</title>
  <link rel="stylesheet" href="style.css" />
</head>s
<body>
<h1>Alumnes</h1>
<table border="2px solid orange">
        <thead>
        <tr>
            <th>Foto</th>
            <th>Nom</th>
            <th>Cognoms</th>
            <th>Edat</th>
        </tr>
        </thead>
        <tbody>
        {
            for $x in doc("institut2.xml")/institut/alumnes/alumne
            order by $x
            return
                 <tr>
       <td>
        {
            let $image := $x/image
            return
            <img  src="{ data($image)}" width="70%"></img>
        }
       </td>
          <td>
          { $x/nom}
          </td>
          <td>
          { $x/cognoms}
          </td>
          <td>
          { $x/edat}
          </td>
          </tr>
    }
    </tbody>
     </table>
</body>
</html>