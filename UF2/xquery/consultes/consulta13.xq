let $doc := doc('institut.xml')
for $i in $doc/institut/alumnes/alumne
let $j := $i/edat
order by $i/nom descending
return ($i/nom, $j)