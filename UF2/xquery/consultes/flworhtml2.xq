

<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Alumnes</title>
</head>
<body>
    <h1>Alumnes</h1>
    <table border="2px solid orange">
        <caption>Alumnes</caption>
        <tr>
            <th>Nom</th>
            <th>Cognom</th>
            <th>Edat</th>
        </tr>
        {
        for $x in doc("institut.xml")/institut/alumnes/alumne
        order by $x
        return
        <tr>
            <td>
                {$x/nom}
            </td>
            <td>
                {$x/cognoms}
            </td>
            <td>
                {$x/edat}
            </td>
        </tr>
        }
    </table>
</body>
</html>
