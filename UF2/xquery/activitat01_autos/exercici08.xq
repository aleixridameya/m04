for $i in doc("autos.xml")/autos/vehicles/vehicle
order by number($i/price) descending
return data($i/price)