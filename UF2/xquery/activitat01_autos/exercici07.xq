for $i in doc("autos.xml")/autos/vehicles/vehicle
where $i/year>2007
order by $i/year descending
return ($i/model, $i/year)