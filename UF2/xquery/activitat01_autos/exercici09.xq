for $i in doc("autos.xml")/autos/vehicles/vehicle
order by $i/price descending
return (data($i/model), data($i/price))