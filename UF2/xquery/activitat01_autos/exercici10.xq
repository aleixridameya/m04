for $i in doc("autos.xml")/autos/vehicles/vehicle
where $i/price>20000
order by $i/price
return (concat(data($i/model), " - ", data($i/price)))