for $i in doc("playstore.xml")/playstore/games/game
where contains(lower-case($i/name),"super")
return ($i/name, $i/company)