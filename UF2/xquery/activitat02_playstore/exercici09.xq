declare function local:expenses($price as xs:decimal) as xs:decimal {
  let $resultat := $price * 0.21 +2.5
  return round($resultat,2)
};

for $i in doc("playstore.xml")/playstore/games/game 
return
<product>
  {$i/name}
  {$i/price}
  <expenses>{local:expenses($i/price)}</expenses>
</product>