for $i in doc("playstore.xml")/playstore/games/game
where $i/age>17
return
<adult_game>
  {$i/name}
  {$i/price}
</adult_game>