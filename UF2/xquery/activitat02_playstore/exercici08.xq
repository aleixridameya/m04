for $i in doc("playstore.xml")/playstore/games/game 
order by $i/price 
return if($i/price<50)
then
  (<offer>{data($i/price)}</offer>)
else
  (<standard>{data($i/price)}</standard>)