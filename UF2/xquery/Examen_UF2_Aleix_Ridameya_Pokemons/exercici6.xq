<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Book list</title>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
    <h1>Pokemons</h1>
    <table>
        <tr>
            <th>Image</th>
            <th>Pokemons</th>
            <th>HP</th>
            <th>ATK</th>
            <th>DEF</th>
            <th>SPD</th>
            <th>SATK</th>
            <th>SDEF</th>
        </tr>
        {
         for $i in doc("pokedex.xml")/pokedex/pokemon
         return
         <tr>
           <td>
           {
             let $imatge := $i/image
             return
             <img src="{ data($imatge)}"></img>
           }
           </td>
           <td>
             {data($i/species)}
           </td>
           <td>
             {data($i/baseStats/HP)}
           </td>
           <td>
             {data($i/baseStats/ATK)}
           </td>
           <td>
             {data($i/baseStats/DEF)}
           </td>
           <td>
             {data($i/baseStats/SPD)}
           </td>
           <td>
             {data($i/baseStats/SATK)}
           </td>
           <td>
             {data($i/baseStats/SDEF)}
           </td>
         </tr>
        }
      </table>
</body>
</html>