for $i in doc("library.xml")/library/book
where $i/@year > 1992 and $i/editorial="Addison-Wesley"
return ($i)