for $i in doc("library.xml")/library/book
where count($i/author) > 1
return ($i/title, data($i/@year))