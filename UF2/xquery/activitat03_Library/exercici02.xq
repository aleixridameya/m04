for $i in doc("library.xml")/library/book
order by $i/@year
return ($i/title,$i/@year)