
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Book list</title>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
    <h1>Book list</h1>
    <table>
        <tr>
            <th>TITLE</th>
            <th>YEAR</th>
            <th>AUTHORS</th>
            <th>EDITORIAL</th>
            <th>PRICE</th>
        </tr>
        {
        for $x in doc("bookstore.xml")/bookstore/book
        where ($x/author/lastname='Stevens')
        return     
        (<tr>
            <td>
                {data($x/title)}
            </td>
            <td>
                {data($x/@year)}
            </td>
            <td>
              {
                for $author in $x/author
                return
                  if (position() eq last()) then
                    (data($author/name),' ',data($author/lastname),<br/>)
              }
            </td>
             <td>
                {$x/editorial}
            </td>
             <td>
                {$x/price}
            </td>
        </tr>)}
        <tr>
          <td colspan="3"></td>
          <td>Total price: </td>
          <td>
          {
            let $books := /bookstore/book[author/lastname = 'Stevens']
            return
              sum($books/price)
          }
          </td>
        </tr>
    </table>
</body>
</html>