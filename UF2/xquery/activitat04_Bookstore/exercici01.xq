<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Book list</title>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
    <h1>Book list</h1>
    <table border="2px solid orange">
        <tr>
            <th>TITLE</th>
            <th>EDITORIAL</th>
            <th>PRICE</th>
        </tr>
        {
        for $x in doc("bookstore.xml")/bookstore/book
        order by $x
        return
        <tr>
            <td>
                {$x/title}
            </td>
            <td>
                {$x/editorial}
            </td>
            <td>
                {$x/price}
            </td>
        </tr>
        }
    </table>
</body>
</html>