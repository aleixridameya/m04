<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>XSL Transform</title>
                <meta charset="UTF-8" />
                <link rel="stylesheet" href="style.css"/>
            </head>
            <body>
                  <table>
                        <tr>
                            <th>Logo</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>License</th>
                        </tr>
                    
                        <xsl:for-each select="coding/programs/program">
                            <tr>
                                <td><xsl:variable name="logotip" select="logo"/>
                                    <img src="{$logotip}"/></td>
                                <td><b>
                                    <xsl:value-of select="name"/></b>
                                </td>
                                <td>
                                    <xsl:value-of select="type"/>
                                </td>
                                <td>
                                    <xsl:value-of select="license"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                
                   </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>