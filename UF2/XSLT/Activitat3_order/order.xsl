<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>XSL Transform</title>
                <meta charset="UTF-8" />
                <link rel="stylesheet" href="style.css"/>
            </head>
            <h3>Products with a Price 25 and Price 100</h3>
            <body>
                  <table class="table1">
                        <tr>
                            <th class="title1" colspan="2">CUSTOMER DETAILS</th>
                        </tr>
                        <xsl:for-each select="order/destination">
                            <tr>
                                <td class="gris">Name</td>
                                <td class="Nom"><xsl:value-of select="name"/>                                </td>
                            </tr>
                            <tr>
                                <td class="gris">Address</td>
                                <td class="td_1"><xsl:value-of select="address"/></td>
                            </tr>
                            <tr>
                                <td class="gris">City</td>
                                <td class="td_1"><xsl:value-of select="city"/></td>
                            </tr>
                            <tr>
                                <td class="gris">P.C.</td>
                                <td class="td_1"><xsl:value-of select="postalcode"/></td>
                            </tr>
                        </xsl:for-each>
                   </table>
                   <br/><br/>
                <table class="table2">
                    <tr>
                        <th class="title2" colspan="4">ORDER</th>
                    </tr>
                    <tr>
                        <th class="subtitle">Product</th>
                        <th class="subtitle">Price</th>
                        <th class="subtitle">Quantity</th>
                        <th class="subtitle">Total</th>
                    </tr>
                    <xsl:for-each select="order/products/product">
                    <xsl:sort select="name"/>
                    <xsl:if test="price >25 and price <= 100">
                    <xsl:choose> 
                        <xsl:when test="price > 25 and price < 50 ">    
                        <tr>
                            <td bgcolor="yellow" class="td_2">
                                <xsl:value-of select="name"/>
                                (code =
                                <xsl:value-of select="@code"/>
                                )
                            </td>
                            <td bgcolor="yellow" class="td_2"><xsl:value-of select="price"/></td>
                            <td bgcolor="yellow" class="td_2"><xsl:value-of select="quantity"/></td>
                            <td bgcolor="yellow" class="td_2"><xsl:value-of select="price * quantity"/></td>
                        </tr>
                        </xsl:when>
                        <xsl:when test="price >= 50 and price < 75 ">    
                        <tr>
                            <td bgcolor="green" class="td_2">
                                <xsl:value-of select="name"/>
                                (code =
                                <xsl:value-of select="@code"/>
                                )
                            </td>
                            <td bgcolor="green" class="td_2"><xsl:value-of select="price"/></td>
                            <td bgcolor="green" class="td_2"><xsl:value-of select="quantity"/></td>
                            <td bgcolor="green" class="td_2"><xsl:value-of select="price * quantity"/></td>
                        </tr>
                        </xsl:when>
                        <xsl:otherwise>
                        <tr>
                            <td bgcolor="red" class="td_2">
                                <xsl:value-of select="name"/>
                                (code =
                                <xsl:value-of select="@code"/>
                                )
                            </td>
                            <td bgcolor="red" class="td_2"><xsl:value-of select="price"/></td>
                            <td bgcolor="red" class="td_2"><xsl:value-of select="quantity"/></td>
                            <td bgcolor="red" class="td_2"><xsl:value-of select="price * quantity"/></td>
                        </tr>
                        </xsl:otherwise>
                    </xsl:choose>
                    </xsl:if>                        
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>