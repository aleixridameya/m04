<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>Atributte img</title>
                <meta charset="UTF-8"/>
                    <style>
                        table, td, th {
                            border:solid 1px;
                            border-collapse: collapse;
                        }
                        th {
                            padding: 23px;
                            background-color: rgb(49, 150, 133);
                        }
                    </style>
            </head>
            <body>
                <h1>Film List</h1>
                <table>
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th >Language</th>
                            <th >Year</th>
                            <th >Country</th>
                            <th >Genre</th>
                            <th >Summary</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="films/film">
                            <tr>
                                <td>
                                    <xsl:value-of select="title"/>
                                </td>
                                <td>
                                    <xsl:value-of select="title/@lang"/>
                                </td>
                                <td>
                                    <xsl:value-of select="year"/>
                                </td>
                                <td>
                                    <xsl:value-of select="country"/>
                                </td>
                                <td>
                                    <xsl:value-of select="genre"/>
                                </td>
                                 <td>
                                    <xsl:value-of select="summary"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>