<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/">
        <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>XSL Transform</title>
                <meta charset="UTF-8" />
                <link rel="stylesheet" href="style.css"/>
            </head>
            <h3>Bookstore</h3>
        </html>
        <xsl:for-each select="bookstore/book">
        <xsl:sort select="category"/>
        <table>
                <tr>
                    <td class="blue">Title (lang: <xsl:value-of select="title/@lang"/>)</td>
                    <td class="td_1"><xsl:value-of select="title"/></td>
                </tr>
                <tr>
                    <td class="blue">Category</td>
                    <td class="td_1"><xsl:value-of select="@category"/></td>
                </tr>
                <tr>
                    <td class="blue">Year</td>
                    <td class="td_1"><xsl:value-of select="year"/></td> 
                </tr>
                <tr>
                    <td class="blue">Price</td>
                    <td class="td_1"><xsl:value-of select="price"/></td>
                </tr>
                <tr>
                    <td class="blue">Format</td>
                    <td class="td_1"><xsl:value-of select="format/@type"/></td>
                </tr>
                <tr>
                    <td class="blue">ISBN</td>
                    <td class="td_1"><xsl:value-of select="isbn"/></td>
                </tr>
                <tr>
                    <td colspan="2" class="author">Authors</td>
                </tr>
                <tr>
                    <td colspan="2" class="author2">
                    <xsl:for-each select="author">
                        <xsl:value-of select="."/>
                            <br/>
                            <xsl:if test="position() != last()">
                        </xsl:if>
                    </xsl:for-each>
                    </td>
                </tr>
                <br/>
            </table>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>