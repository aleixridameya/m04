<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output method="html" indent="yes"/>
    <xsl:template match="/" >
        <html>
            <head>
                <title>XSL Transform</title>
                <meta charset="UTF-8" />
            </head>
            <body>
                <h2>Books</h2>
                <xsl:apply-templates select="//book"/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="book">
        <p><xsl:value-of select="title" /></p>
    </xsl:template>
</xsl:stylesheet>