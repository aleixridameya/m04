

function convertir() {
    let temp = document.getElementById("temperatura").value;
    let mesura = document.getElementById("mesura").value;
    let mesura_canvi = document.getElementById("mesura_canvi").value;

    
    let resultat;
    let formula;
    
    if (mesura == "C" && mesura_canvi == "F") {
        formula = "F = C * (9/5) +32";
        resultat = temp * (9/5) + 32
    }

    else if (mesura == "C" && mesura_canvi == "K") {
        formula = "K = C + 273.15";
        resultat = temp * 273.15
    }
    
    else if (mesura == "C" && mesura_canvi == "R") {
        formula = "R = C * (9/5) + 32 + 459.67";
        resultat = temp * (9/5) + 32 + 459.67
    }

    else if (mesura == "F" && mesura_canvi == "C") {
        formula = "C = (F - 32) * 5/9";
        resultat = (temp - 32) * 5/9
    }

    else if (mesura == "F" && mesura_canvi == "K") {
        formula = "K = (F - 32) * 5/9 + 273.15";
        resultat = (temp - 32) * 5/9 + 273.15
    }

    else if (mesura == "F" && mesura_canvi == "R") {
        formula = "R = F + 459.67";
        resultat = temp + 459.67
    }

    else if (mesura == "K" && mesura_canvi == "C") {
        formula = "C = K - 273.15";
        resultat = temp - 273.15
    }

    else if (mesura == "K" && mesura_canvi == "F") {
        formula = "F = K * (9/5) - 459.67";
        resultat = temp * (9/5) - 459.67
    }

    else if (mesura == "K" && mesura_canvi == "R") {
        formula = "R = K * 9/5";
        resultat = temp * 9/5
    }

    else if (mesura == "R" && mesura_canvi == "C") {
        formula = "C = (R - 32 - 459.67) * 9/5";
        resultat = (temp - 32 - 459.67) * 9/5
    }

    else if (mesura == "R" && mesura_canvi == "F") {
        formula = "F = R - 459.67";
        resultat = temp - 459.67
    }

    else if (mesura == "R" && mesura_canvi == "K") {
        formula = "K = R * 5/9";
        resultat = temp * 5/9
    }

    document.getElementById("formula").innerHTML = formula;
    document.getElementById("resultat").value = resultat.toFixed(2);

}



