
let myhand = '';
let pchand = '';

let punts_jugador = 0;
let punts_ordinador = 0;

function random(){
    
    let llista = ['rock','paper','scissors','lizard','spock']

    for (let index = 0; index < llista.length; index++) {
       
        let imatge = llista[index]

         setTimeout(function(imatge) {
            return function(){
                document.getElementById("resultat2").src = "./" +  imatge + ".png";           
            }; 
        
        }(imatge),index * 75);
    

    }

    setTimeout( function(){
        pc_selection();
    }, llista.length * 75);

}

function pc_selection() {
    const ran = Math.floor(Math.random() * 5);

    if (ran == 0 ) {
        pchand = 'rock';
        document.getElementById("resultat2").src = "rock.png";
    }

    else if (ran == 1 ) {
        pchand = 'paper';
        document.getElementById("resultat2").src = "paper.png";
    }

    else if (ran == 2 ) {
        pchand = 'scissors';
        document.getElementById("resultat2").src = "scissors.png";
    }

    else if (ran == 3 ) {
        pchand = 'lizard';
        document.getElementById("resultat2").src = "lizard.png";
    }

    else if (ran == 4 ) {
        pchand = 'spock';
        document.getElementById("resultat2").src = "spock.png";
    }

    comparacio()

}

function canvi(mano){
    document.getElementById("resultat1").src = "./" +  mano + ".png";
    myhand = mano;
}


function comparacio() {
    if ( myhand == pchand ) {
        document.getElementById("resultat").innerText = "Empat";
    }

    else if (
           
        (myhand == "rock" && (pchand == "scissors" || pchand == "lizard")) || 
        (myhand == "paper" && (pchand == "rock" || pchand == "spock")) ||
        (myhand == "scissors" && (pchand == "paper" || pchand == "lizard")) ||
        (myhand == "lizard" && (pchand == "spock" || pchand == "paper")) ||
        (myhand == "spock" && (pchand == "rock" || pchand == "scissors"))
    ) {
        document.getElementById("resultat").innerText = "El jugador ha guanyat";
        punts_jugador++;
    }
    
    else {
        document.getElementById("resultat").innerText = "El ordinador ha guanyat";
        punts_ordinador++;
    }

    document.getElementById("punts_jugador").innerText = "Points: " + punts_jugador ;
    document.getElementById("punts_ordinador").innerText = "Points: " + punts_ordinador ;

};